package pwprotect.views;

import java.awt.FlowLayout;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import pwprotect.controllers.PasswordController;
import pwprotect.model.PlainPassword;

public class PasswordView extends JFrame {

    private PasswordController controller;
    private PlainPassword password;

    JLabel jlb1 = new JLabel("Usuario: ");
    JLabel jlb2 = new JLabel("Senha: ");
    JLabel jlb3 = new JLabel("Confirmar: ");
    JLabel jlb4 = new JLabel("Tags: ");
    JTextField jtf1 = new JTextField("", 30);
    JTextField jtf2 = new JTextField("", 30);
    JPasswordField jpf1 = new JPasswordField("", 30);
    JPasswordField jpf2 = new JPasswordField("", 30);

    JButton jbtn1 = new JButton("Generate");
    JButton jbtn2 = new JButton("Cancel");
    JButton jbtn3 = new JButton("Save");

    public PasswordView() {
        this.setLayout(new FlowLayout());
        this.setSize(370, 320);

        this.getContentPane().add(jlb1);
        this.getContentPane().add(jtf1);
        this.getContentPane().add(jlb2);
        this.getContentPane().add(jpf1);
        jbtn1.setActionCommand("generate");
        this.getContentPane().add(jbtn1);

        this.getContentPane().add(jlb3);
        this.getContentPane().add(jpf2);
        this.getContentPane().add(jlb4);
        this.getContentPane().add(jtf2);

        jbtn2.setActionCommand("cancel");
        this.getContentPane().add(jbtn2);
        jbtn3.setActionCommand("save");
        this.getContentPane().add(jbtn3);

        jbtn1.addActionListener(event -> {
            String senha = controller.gerarSenha();
            jpf1.setText(senha);
            jpf2.setText(senha);
        });

        jbtn2.addActionListener(event -> {
            this.setVisible(false);
        });
        jbtn3.addActionListener(event -> {
            System.out.println("Clicou");
            if (!Arrays.equals(jpf1.getPassword(), jpf2.getPassword())) {
                JOptionPane.showMessageDialog(this, "Passwords don't match");
                return;
           }
            
            password.setName(jtf1.getText());
            password.setPassword(new String(jpf1.getPassword()));
            password.setTagList(jtf2.getText());
            controller.save(this.password.getEncryptedPassword());
        });
    }

    public void update() {
        jtf1.setText(orDefault(password.getName(), ""));
        jpf1.setText(orDefault(password.getPassword(), ""));
        jpf2.setText(orDefault(password.getPassword(), ""));
        jtf2.setText(orDefault(password.getTagList(), ""));
    }
    
    private <T> T orDefault(T obj, T def) {
        return obj == null ? def : obj;
    }

    public PasswordController getController() {
        return controller;
    }

    public void setController(PasswordController controller) {
        this.controller = controller;
    }

    public PlainPassword getPassword() {
        return password;
    }

    public void setPassword(PlainPassword password) {
        this.password = password;
    }

    public JButton getJbtn1() {
        return jbtn1;
    }

    public void setJbtn1(JButton jbtn1) {
        this.jbtn1 = jbtn1;
    }

    public JButton getJbtn2() {
        return jbtn2;
    }

    public void setJbtn2(JButton jbtn2) {
        this.jbtn2 = jbtn2;
    }

    public JButton getJbtn3() {
        return jbtn3;
    }

    public void setJbtn3(JButton jbtn3) {
        this.jbtn3 = jbtn3;
    }

    public JPasswordField getJpf1() {
        return jpf1;
    }

    public void setJpf1(JPasswordField jpf1) {
        this.jpf1 = jpf1;
    }

    public JPasswordField getJpf2() {
        return jpf2;
    }

    public void setJpf2(JPasswordField jpf2) {
        this.jpf2 = jpf2;
    }
}
