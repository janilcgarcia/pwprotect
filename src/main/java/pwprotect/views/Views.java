package pwprotect.views;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

import javax.swing.JFrame;

public class Views extends JFrame {

    private LoginView login;
    private UserView user;
    private PasswordListView passwordList;
    private PasswordView password;

    public Views() {
        this.login = new LoginView();
        this.user = new UserView();
        this.passwordList = new PasswordListView();
        this.password = new PasswordView();

        // login.setVisible(true);
        // user.setVisible(true);
        // passwordList.setVisible(true);
        // password.setVisible(true);
    }

    public LoginView getLogin() {
        return login;
    }

    public UserView getUser() {
        return user;
    }

    public PasswordListView getPasswordList() {
        return passwordList;
    }

    public PasswordView getPassword() {
        return password;
    }

    public void toClipboard(String value) {
        Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
        clip.setContents(new StringSelection(value), null);
    }
}
