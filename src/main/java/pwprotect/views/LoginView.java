package pwprotect.views;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import pwprotect.controllers.UserController;

public class LoginView extends JFrame {

    private UserController controller;

    JLabel jlb1 = new JLabel("Usuario: ");
    JLabel jlb2 = new JLabel("Senha: ");
    JTextField jtf1 = new JTextField("", 20);
    JPasswordField jtf2 = new JPasswordField("", 20);

    JButton jbtn1 = new JButton("Login");
    JButton jbtn2 = new JButton("Create");

    public LoginView() {
        this.setLayout(new FlowLayout());
        this.setSize(325, 150);

        this.getContentPane().add(jlb1);
        this.getContentPane().add(jtf1);
        this.getContentPane().add(jlb2);
        this.getContentPane().add(jtf2);

        jbtn1.setActionCommand("login");
        this.getContentPane().add(jbtn1);
        jbtn2.setActionCommand("create");
        this.getContentPane().add(jbtn2);

        // Create actions
        jbtn1.addActionListener(event -> {
            controller.login(jtf1.getText(), new String(jtf2.getPassword()));
            jtf2.setText("");
        });

        jbtn2.addActionListener(event -> controller.create());
    }

    public void update() {
    }

    public UserController getController() {
        return controller;
    }

    public void setController(UserController controller) {
        this.controller = controller;
    }

    public JButton getJbtn1() {
        return jbtn1;
    }

    public JButton getJbtn2() {
        return jbtn2;
    }

    public JTextField getJtf1() {
        return jtf1;
    }

    public JTextField getJtf2() {
        return jtf2;
    }
}
