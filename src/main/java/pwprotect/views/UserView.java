package pwprotect.views;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.bouncycastle.util.Arrays;

import pwprotect.controllers.UserController;
import pwprotect.model.CryptoContext;
import pwprotect.model.User;

public class UserView extends JFrame {

    private UserController controller;
    private User user;

    JLabel jlb1 = new JLabel("Usuario: ");
    JLabel jlb2 = new JLabel("Senha Atual: ");
    JLabel jlb3 = new JLabel("Nova Senha: ");
    JLabel jlb4 = new JLabel("Confirmar Senha: ");
    JTextField jtf1 = new JTextField("", 20);
    JPasswordField jpf1 = new JPasswordField("", 20);
    JPasswordField jpf2 = new JPasswordField("", 20);
    JPasswordField jpf3 = new JPasswordField("", 20);

    JButton jbtn1 = new JButton("Delete");
    JButton jbtn2 = new JButton("Cancel");
    JButton jbtn3 = new JButton("Save");

    public UserView() {
        this.setLayout(new FlowLayout());
        this.setSize(290, 280);

        this.getContentPane().add(jlb1);
        this.getContentPane().add(jtf1);

        this.getContentPane().add(jlb2);
        this.getContentPane().add(jpf1);

        this.getContentPane().add(jlb3);
        this.getContentPane().add(jpf2);
        this.getContentPane().add(jlb4);
        this.getContentPane().add(jpf3);

        jbtn1.setActionCommand("delete");
        this.getContentPane().add(jbtn1);
        jbtn2.setActionCommand("cancel");
        this.getContentPane().add(jbtn2);
        jbtn3.setActionCommand("save");
        this.getContentPane().add(jbtn3);

        jbtn1.addActionListener(event -> controller.delete(user));
        jbtn2.addActionListener(event -> setVisible(false));
        jbtn3.addActionListener(event -> {
            user.setName(jtf1.getText());

            char[] original = jpf1.getPassword();
            char[] password = jpf2.getPassword();
            char[] verify = jpf3.getPassword();

            if (!Arrays.areEqual(password, verify)) {
                JOptionPane.showMessageDialog(this, "Verificação da senha é inválida");
                return;
            }

            CryptoContext context = new CryptoContext(user);
            context.changePassword(user.getId() == null ? null : new String(original), new String(password));

            controller.save(user);
        });
    }

    public void update() {
        if (user != null) {
            jtf1.setText(user.getName() != null ? user.getName() : "");

            if (user.getId() != null) {
                jbtn1.setEnabled(true);
                jlb2.setVisible(true);
                jpf1.setVisible(true);
            } else {
                jbtn1.setEnabled(false);
                jlb2.setVisible(false);
                jpf1.setVisible(false);
            }
        }
        
        jpf1.setText("");
        jpf2.setText("");
        jpf3.setText("");
    }

    public UserController getController() {
        return controller;
    }

    public void setController(UserController controller) {
        this.controller = controller;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public JButton getJbtn1() {
        return jbtn1;
    }

    public void setJbtn1(JButton jbtn1) {
        this.jbtn1 = jbtn1;
    }

    public JButton getJbtn2() {
        return jbtn2;
    }

    public void setJbtn2(JButton jbtn2) {
        this.jbtn2 = jbtn2;
    }

    public JButton getJbtn3() {
        return jbtn3;
    }

    public void setJbtn3(JButton jbtn3) {
        this.jbtn3 = jbtn3;
    }

}
