package pwprotect.model;

import java.util.HashSet;
import java.util.Set;

public class User {

    private Long id;
    private String name;
    private String pwHashSalt;
    private String pwHash;
    private String secretKeyIv;
    private String secretKey;
    private int iterations;
    private Set<EncryptedPassword> passwords;

    public static final int CURRENT_ITERATIONS = 1 << 18;

    public User(Long id, String name, String pwHashSalt, String pwHash, String secretKeyIv, String secretKey,
            int iterations, Set<EncryptedPassword> passwords) {
        this.id = id;
        this.name = name;
        this.pwHashSalt = pwHashSalt;
        this.pwHash = pwHash;
        this.secretKeyIv = secretKeyIv;
        this.secretKey = secretKey;
        this.iterations = iterations;
        this.passwords = passwords;
    }

    public User(String name, String pwHashSalt, String pwHash, String secretKeyIv, String secretKey, int iterations,
            Set<EncryptedPassword> passwords) {
        this(null, name, pwHashSalt, pwHash, secretKeyIv, secretKey, iterations, passwords);
    }

    public User() {
        this(null, null, null, null, null, null, User.CURRENT_ITERATIONS, new HashSet<>());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwHashSalt() {
        return pwHashSalt;
    }

    public void setPwHashSalt(String pwHashSalt) {
        this.pwHashSalt = pwHashSalt;
    }

    public String getPwHash() {
        return pwHash;
    }

    public void setPwHash(String pwHash) {
        this.pwHash = pwHash;
    }

    public String getSecretKeyIv() {
        return secretKeyIv;
    }

    public void setSecretKeyIv(String secretKeyIv) {
        this.secretKeyIv = secretKeyIv;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public Set<EncryptedPassword> getPasswords() {
        return passwords;
    }

    public void setPasswords(Set<EncryptedPassword> passwords) {
        this.passwords = passwords;
    }
}
