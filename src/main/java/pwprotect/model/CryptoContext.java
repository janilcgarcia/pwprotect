package pwprotect.model;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.crypto.modes.OFBBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.util.encoders.Base64;

public class CryptoContext {

    private final User user;
    private KeyParameter keyParameter;
    private SecureRandom random;

    public CryptoContext(User user) {
        this.user = user;
        this.keyParameter = null;
        this.random = null;
    }

    public void openContext(String password) {
        if (!checkPassword(password)) {
            throw new SecurityException("Invalid password for user " + user.getName());
        }

        // Initialize cryptography objects
        PKCS5S2ParametersGenerator pbkdf2 = new PKCS5S2ParametersGenerator(new SHA512Digest());
        OFBBlockCipher ofb = new OFBBlockCipher(new AESEngine(), 128);

        // Decode base64 data
        final byte[] passwordBytes = password.getBytes(StandardCharsets.UTF_8);
        final byte[] pwHash = Base64.decode(user.getPwHash());
        final byte[] secretKeyIv = Base64.decode(user.getSecretKeyIv());
        final byte[] secretKey = Base64.decode(user.getSecretKey());

        // Generate key from password
        pbkdf2.init(passwordBytes, pwHash, user.getIterations());

        ofb.init(false, new ParametersWithIV(pbkdf2.generateDerivedParameters(256), secretKeyIv));

        // Load key
        byte[] key = new byte[32];
        ofb.processBytes(secretKey, 0, 32, key, 0);
        keyParameter = new KeyParameter(key);
    }

    public PlainPassword decryptPassword(EncryptedPassword password) {
        if (!isOpen()) {
            throw new IllegalStateException("Context is not open");
        }

        return new PlainPassword(password, keyParameter);
    }

    public void closeContext() {
        this.keyParameter = null;
    }

    public boolean isOpen() {
        return this.keyParameter != null;
    }

    /**
     * Check if the input password's hash matches the stored password's hash.
     *
     * @param password The input password to be checked. The UTF8 bytes of this
     * string are used.
     * @return If the hashes matched, return true, else false.
     * @throws IllegalStateException Throws exception if there is no password
     * set on this user.
     * @throws NullPointerException May throw NullPointerException if password
     * is null.
     */
    public boolean checkPassword(String password) {
        if (user.getPwHashSalt() == null || user.getPwHash() == null) {
            throw new IllegalStateException("You can only check the " + "password if one is present");
        }

        PKCS5S2ParametersGenerator pbkdf2 = new PKCS5S2ParametersGenerator(new SHA512Digest());

        final byte[] passwordBytes = password.getBytes(StandardCharsets.UTF_8);
        final byte[] pwHashSalt = Base64.decode(user.getPwHashSalt());
        final byte[] pwHash = Base64.decode(user.getPwHash());

        pbkdf2.init(passwordBytes, pwHashSalt, user.getIterations());

        final KeyParameter key = (KeyParameter) pbkdf2.generateDerivedParameters(512);

        return Arrays.equals(key.getKey(), pwHash);
    }

    /**
     * Set the user password's PBKDF2(SHA512) hash, that can checked later. This
     * function also sets the PBKDF2(SHA512) parameter to the best performance x
     * security current case.
     *
     * @param password The password to be hashed. To be used in the algorithm,
     * it's contents are converted to UTF8 bytes.
     */
    private void setPassword(String password) {
        // Initialize cryptographic primitives
        final SecureRandom secureRandom = getRandom();
        final PKCS5S2ParametersGenerator pbkdf2 = new PKCS5S2ParametersGenerator(new SHA512Digest());

        // Create byte storage
        final byte[] salt = new byte[64];
        final byte[] pwBytes = password.getBytes(StandardCharsets.UTF_8);

        // Create hash
        secureRandom.nextBytes(salt);

        pbkdf2.init(pwBytes, salt, User.CURRENT_ITERATIONS);
        KeyParameter key = (KeyParameter) pbkdf2.generateDerivedParameters(512);

        // Set user
        user.setIterations(User.CURRENT_ITERATIONS);
        user.setPwHash(Base64.toBase64String(key.getKey()));
        user.setPwHashSalt(Base64.toBase64String(salt));
    }

    public void changePassword(String oldPassword, String newPassword) {
        // Get secret or create a new one
        SecureRandom secureRandom = getRandom();
        byte[] secret;

        if (user.getSecretKey() == null) {
            secret = new byte[32];
            secureRandom.nextBytes(secret);
        } else if (oldPassword == null) {
            throw new SecurityException("Old password is necessary to change " + "current password");
        } else {
            openContext(oldPassword);
            secret = keyParameter.getKey().clone();
            closeContext();
        }

        // Update password hash
        setPassword(newPassword);

        // Generate key
        final byte[] passwordBytes = newPassword.getBytes(StandardCharsets.UTF_8);
        final byte[] passwordHash = Base64.decode(user.getPwHash());

        final PKCS5S2ParametersGenerator pbkdf2 = new PKCS5S2ParametersGenerator(new SHA512Digest());

        pbkdf2.init(passwordBytes, passwordHash, user.getIterations());
        final KeyParameter key = (KeyParameter) pbkdf2.generateDerivedParameters(256);

        // Generate IV
        final byte[] iv = new byte[16];
        secureRandom.nextBytes(iv);

        // Encrypt secret
        final byte[] encrypted = new byte[32];
        OFBBlockCipher ofb = new OFBBlockCipher(new AESEngine(), 128);

        ofb.init(true, new ParametersWithIV(key, iv));

        ofb.processBytes(secret, 0, 32, encrypted, 0);

        // Set user
        user.setSecretKeyIv(Base64.toBase64String(iv));
        user.setSecretKey(Base64.toBase64String(encrypted));
    }

    private SecureRandom getRandom() {
        if (this.random == null) {
            try {
                this.random = SecureRandom.getInstance("NativePRNG");
            } catch (NoSuchAlgorithmException ex) {
                this.random = new SecureRandom();
            }
        }

        return this.random;
    }
}
