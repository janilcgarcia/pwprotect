package pwprotect.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import pwprotect.model.CryptoContext;
import pwprotect.model.EncryptedPassword;
import pwprotect.model.PlainPassword;
import pwprotect.model.User;
import pwprotect.views.LoginView;
import pwprotect.views.PasswordListView;
import pwprotect.views.PasswordView;
import pwprotect.views.UserView;
import pwprotect.views.Views;

public class PasswordController implements ActionListener {

    private Views views;
    private CryptoContext cryptoContext;
    private User user;
    private SessionFactory sessionFactory;
    private String filter;

    public PasswordController(Views views, User user, CryptoContext context, SessionFactory factory) {
        this.views = views;
        this.cryptoContext = context;
        this.user = user;
        this.sessionFactory = factory;
        
        views.getPasswordList().setController(this);
        views.getPassword().setController(this);
    }

    private void listLike() {
        PasswordListView passwordListView = views.getPasswordList();

        Session s = sessionFactory.openSession();

        try {
            CriteriaBuilder builder = s.getCriteriaBuilder();
            CriteriaQuery<EncryptedPassword> criteria = builder.createQuery(EncryptedPassword.class);
            Root<EncryptedPassword> root = criteria.from(EncryptedPassword.class);
            criteria.select(root);
            if (this.filter != null) {
                criteria.where(builder.or(builder.like(root.get("name"), this.filter),
                        builder.like(root.get("tagList"), this.filter)));
            }

            List<EncryptedPassword> passwords = s.createQuery(criteria).getResultList();

            passwordListView.setPasswords(passwords);
            passwordListView.update();
        } finally {
            s.close();
        }
    }

    public void list(String filter) {
        if (filter != null && !filter.isEmpty()) {
            this.filter = "%" + filter + "%";
        } else {
            this.filter = null;
        }
        listLike();
    }

    public void create() {
        PasswordView pwView = views.getPassword();
        PlainPassword plain = cryptoContext.decryptPassword(new EncryptedPassword());
        plain.setUser(user);

        pwView.setPassword(plain);
        pwView.setVisible(true);
        pwView.update();
    }

    public void edit(EncryptedPassword password) {
        PasswordView pwView = views.getPassword();
        pwView.setPassword(cryptoContext.decryptPassword(password));
        pwView.setVisible(true);
        pwView.update();
    }

    public void save(EncryptedPassword password) {
        PasswordView pwView = views.getPassword();
        Session s = sessionFactory.openSession();
        Transaction tx = null;

        try {
            tx = s.beginTransaction();
            s.saveOrUpdate(password);
            tx.commit();

            pwView.setVisible(false);
            listLike();
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
            ex.printStackTrace(System.err);
        } finally {
            s.close();
        }
    }

    public void delete(EncryptedPassword password) {
        Session s = sessionFactory.openSession();
        Transaction tx = null;

        try {
            tx = s.beginTransaction();
            s.delete(password);
            tx.commit();

            listLike();
        } catch (Exception ex) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            s.close();
        }
    }

    public void logout() {
        final PasswordView pwView = views.getPassword();
        final PasswordListView pwLView = views.getPasswordList();
        final UserView userView = views.getUser();
        final LoginView loginView = views.getLogin();

        pwView.setController(null);
        pwLView.setController(null);

        pwView.setVisible(false);
        pwLView.setVisible(false);

        userView.setVisible(false);
        loginView.setVisible(true);
    }

    public void editUser() {
        final UserView userView = views.getUser();

        userView.setUser(user);
        userView.setVisible(true);
        userView.update();
    }

    public void copyToClipboard(EncryptedPassword password) {
        PlainPassword plain = cryptoContext.decryptPassword(password);
        views.toClipboard(plain.getPassword());
    }

    public String gerarSenha() {
        List<Integer> array = new ArrayList<>();

        IntStream.rangeClosed('0', '9').forEach(array::add);// 0-9
        IntStream.rangeClosed('A', 'Z').forEach(array::add);// A-Z
        IntStream.rangeClosed('a', 'z').forEach(array::add);// a-z
        IntStream.rangeClosed('!', '*').forEach(array::add);// !-*

        int tamanho = 8;
        SecureRandom random = new SecureRandom();
        StringBuilder senha = new StringBuilder();

        random.ints(tamanho, 0, array.size()).map(array::get).forEach(car -> senha.append((char) car));
        return senha.toString();
    }

    public Views getViews() {
        return views;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("AdicionarNovoUsuario")) {
            create();
        }

        if (e.getActionCommand().equals("ExcluirUsuario")) {
            delete(views.getPasswordList().getPasswords().get(0));
        }

        if (e.getActionCommand().equals("EditarUsuario")) {
            editUser();
        }

        if (e.getActionCommand().equals("generate")) {
        }

        if (e.getActionCommand().equals("cancel")) {
            views.getPassword().setVisible(false);
            views.getPasswordList().setVisible(true);
        }

        if (e.getActionCommand().equals("save")) {
            save(views.getPassword().getPassword().getEncryptedPassword());
        }
    }
}
