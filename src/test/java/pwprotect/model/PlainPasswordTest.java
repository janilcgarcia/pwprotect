package pwprotect.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PlainPasswordTest {

    @Test
    public void testSetPassword() {
        final String userPw = "password123";
        final String storedPw = "storedpw";

        final User u = new User();
        final CryptoContext c = new CryptoContext(u);

        c.changePassword(null, userPw);
        c.openContext(userPw);

        EncryptedPassword enc = new EncryptedPassword();
        PlainPassword plain = c.decryptPassword(enc);

        plain.setPassword(storedPw);

        c.closeContext();

        c.openContext(userPw);
        plain = c.decryptPassword(enc);

        assertEquals(plain.getPassword(), storedPw);
        c.closeContext();
    }
}
